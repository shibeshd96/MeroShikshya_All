-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 20, 2018 at 04:06 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quest2`
--

-- --------------------------------------------------------

--
-- Table structure for table `ans_practice`
--

CREATE TABLE `ans_practice` (
  `id` int(6) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ans_practice`
--

INSERT INTO `ans_practice` (`id`, `status`) VALUES
(0, ''),
(1, 'correct'),
(2, 'correct'),
(3, 'incorrect'),
(4, 'incorrect'),
(5, 'incorrect'),
(6, 'incorrect'),
(7, 'incorrect'),
(8, 'incorrect'),
(9, 'incorrect'),
(10, 'incorrect'),
(11, 'incorrect'),
(12, 'incorrect'),
(13, 'correct'),
(1, 'incorrect'),
(2, 'incorrect'),
(3, 'incorrect'),
(4, 'incorrect'),
(5, 'incorrect'),
(6, 'incorrect'),
(7, 'incorrect'),
(8, 'incorrect'),
(9, 'incorrect'),
(10, 'incorrect'),
(11, 'incorrect'),
(12, 'correct'),
(13, 'incorrect');

-- --------------------------------------------------------

--
-- Table structure for table `ans_test`
--

CREATE TABLE `ans_test` (
  `id` int(6) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ans_test`
--

INSERT INTO `ans_test` (`id`, `status`) VALUES
(0, ''),
(1, 'incorrect'),
(2, 'incorrect'),
(3, 'incorrect'),
(4, 'incorrect'),
(5, 'incorrect'),
(6, 'incorrect'),
(7, 'incorrect'),
(8, 'incorrect'),
(9, 'incorrect'),
(10, 'incorrect'),
(11, 'incorrect'),
(12, 'incorrect'),
(13, 'correct');

-- --------------------------------------------------------

--
-- Table structure for table `quanda`
--

CREATE TABLE `quanda` (
  `id` int(6) UNSIGNED NOT NULL,
  `question` varchar(200) NOT NULL,
  `ans1` varchar(100) NOT NULL,
  `ans2` varchar(100) NOT NULL,
  `ans3` varchar(100) NOT NULL,
  `ans4` varchar(100) NOT NULL,
  `correct` varchar(100) NOT NULL,
  `passage` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quanda`
--

INSERT INTO `quanda` (`id`, `question`, `ans1`, `ans2`, `ans3`, `ans4`, `correct`, `passage`) VALUES
(1, 'Scientific name of mustard is ___________.', 'pissum sativum', 'solanum tuborosum', 'lycoperiscum esculentum', 'brassica campestris', 'ans4', 'My country Nepal is surrounding by two countries. China lies in the east side and west, north and south is cover by India. It is in the northern hemisphere. It has three geographical zones. Himalayan, Mountainous and tarai. We have monsoon climate. It is very cold and dry in winter and hot, stormy and rainy in summer. It is rich in natural beauty and resources. It has long and wide rivers like koshi, Gandaki, and Karnali. We have large lakes like Rupa, Begnas, and Rara etc. like-wise we have green valleys, lovely water falls etc.  It is rich in religious and historical heritages. Lumbini, Gorkha,   Janakpur, Kathmandu are the famous examples.'),
(2, 'Scientific name of tomato is ___________.', 'pissum sativum', 'solanum tuborosum', 'lycoperiscum esculentum', 'brassica campestris', 'ans3', 'My country Nepal is surrounding by two countries. China lies in the east side and west, north and south is cover by India. It is in the northern hemisphere. It has three geographical zones. Himalayan, Mountainous and tarai. We have monsoon climate. It is very cold and dry in winter and hot, stormy and rainy in summer. It is rich in natural beauty and resources. It has long and wide rivers like koshi, Gandaki, and Karnali. We have large lakes like Rupa, Begnas, and Rara etc. like-wise we have green valleys, lovely water falls etc.  It is rich in religious and historical heritages. Lumbini, Gorkha,   Janakpur, Kathmandu are the famous examples.'),
(3, 'Fleshy part of apple is made up of___________.', 'apex cells', 'sclerenchyma', 'parenchyma', 'collenchyma', 'ans2', 'My country Nepal is surrounding by two countries. China lies in the east side and west, north and south is cover by India. It is in the northern hemisphere. It has three geographical zones. Himalayan, Mountainous and tarai. We have monsoon climate. It is very cold and dry in winter and hot, stormy and rainy in summer. It is rich in natural beauty and resources. It has long and wide rivers like koshi, Gandaki, and Karnali. We have large lakes like Rupa, Begnas, and Rara etc. like-wise we have green valleys, lovely water falls etc.  It is rich in religious and historical heritages. Lumbini, Gorkha,   Janakpur, Kathmandu are the famous examples.'),
(4, 'is a equals a?', 'yes', 'no', 'dont know', 'none of the above', 'ans1', 'My country Nepal is surrounding by two countries. China lies in the east side and west, north and south is cover by India. It is in the northern hemisphere. It has three geographical zones. Himalayan, Mountainous and tarai. We have monsoon climate. It is very cold and dry in winter and hot, stormy and rainy in summer. It is rich in natural beauty and resources. It has long and wide rivers like koshi, Gandaki, and Karnali. We have large lakes like Rupa, Begnas, and Rara etc. like-wise we have green valleys, lovely water falls etc.  It is rich in religious and historical heritages. Lumbini, Gorkha,   Janakpur, Kathmandu are the famous examples.'),
(5, 'is b equals b?', 'yes', 'no', 'dont know', 'none of the above', 'ans1', 'My country Nepal is surrounding by two countries. China lies in the east side and west, north and south is cover by India. It is in the northern hemisphere. It has three geographical zones. Himalayan, Mountainous and tarai. We have monsoon climate. It is very cold and dry in winter and hot, stormy and rainy in summer. It is rich in natural beauty and resources. It has long and wide rivers like koshi, Gandaki, and Karnali. We have large lakes like Rupa, Begnas, and Rara etc. like-wise we have green valleys, lovely water falls etc.  It is rich in religious and historical heritages. Lumbini, Gorkha,   Janakpur, Kathmandu are the famous examples.'),
(6, 'is c equals c?', 'yes', 'no', 'dont know', 'none of the above', 'ans1', 'My country Nepal is surrounding by two countries. China lies in the east side and west, north and south is cover by India. It is in the northern hemisphere. It has three geographical zones. Himalayan, Mountainous and tarai. We have monsoon climate. It is very cold and dry in winter and hot, stormy and rainy in summer. It is rich in natural beauty and resources. It has long and wide rivers like koshi, Gandaki, and Karnali. We have large lakes like Rupa, Begnas, and Rara etc. like-wise we have green valleys, lovely water falls etc.  It is rich in religious and historical heritages. Lumbini, Gorkha,   Janakpur, Kathmandu are the famous examples.'),
(7, 'is d equals d?', 'yes', 'no', 'dont know', 'none of the above', 'ans1', 'My country Nepal is surrounding by two countries. China lies in the east side and west, north and south is cover by India. It is in the northern hemisphere. It has three geographical zones. Himalayan, Mountainous and tarai. We have monsoon climate. It is very cold and dry in winter and hot, stormy and rainy in summer. It is rich in natural beauty and resources. It has long and wide rivers like koshi, Gandaki, and Karnali. We have large lakes like Rupa, Begnas, and Rara etc. like-wise we have green valleys, lovely water falls etc.  It is rich in religious and historical heritages. Lumbini, Gorkha,   Janakpur, Kathmandu are the famous examples.'),
(8, 'is e equals e?', 'yes', 'no', 'dont know', 'none of the above', 'ans1', 'My country Nepal is surrounding by two countries. China lies in the east side and west, north and south is cover by India. It is in the northern hemisphere. It has three geographical zones. Himalayan, Mountainous and tarai. We have monsoon climate. It is very cold and dry in winter and hot, stormy and rainy in summer. It is rich in natural beauty and resources. It has long and wide rivers like koshi, Gandaki, and Karnali. We have large lakes like Rupa, Begnas, and Rara etc. like-wise we have green valleys, lovely water falls etc.  It is rich in religious and historical heritages. Lumbini, Gorkha,   Janakpur, Kathmandu are the famous examples.'),
(9, 'is f equals f?', 'yes', 'no', 'dont know', 'none of the above', 'ans1', 'My country Nepal is surrounding by two countries. China lies in the east side and west, north and south is cover by India. It is in the northern hemisphere. It has three geographical zones. Himalayan, Mountainous and tarai. We have monsoon climate. It is very cold and dry in winter and hot, stormy and rainy in summer. It is rich in natural beauty and resources. It has long and wide rivers like koshi, Gandaki, and Karnali. We have large lakes like Rupa, Begnas, and Rara etc. like-wise we have green valleys, lovely water falls etc.  It is rich in religious and historical heritages. Lumbini, Gorkha,   Janakpur, Kathmandu are the famous examples.'),
(10, 'is g equals g?', 'yes', 'no', 'dont know', 'none of the above', 'ans1', 'My country Nepal is surrounding by two countries. China lies in the east side and west, north and south is cover by India. It is in the northern hemisphere. It has three geographical zones. Himalayan, Mountainous and tarai. We have monsoon climate. It is very cold and dry in winter and hot, stormy and rainy in summer. It is rich in natural beauty and resources. It has long and wide rivers like koshi, Gandaki, and Karnali. We have large lakes like Rupa, Begnas, and Rara etc. like-wise we have green valleys, lovely water falls etc.  It is rich in religious and historical heritages. Lumbini, Gorkha,   Janakpur, Kathmandu are the famous examples.'),
(11, 'is h equals h?', 'yes', 'no', 'dont know', 'none of the above', 'ans1', 'My country Nepal is surrounding by two countries. China lies in the east side and west, north and south is cover by India. It is in the northern hemisphere. It has three geographical zones. Himalayan, Mountainous and tarai. We have monsoon climate. It is very cold and dry in winter and hot, stormy and rainy in summer. It is rich in natural beauty and resources. It has long and wide rivers like koshi, Gandaki, and Karnali. We have large lakes like Rupa, Begnas, and Rara etc. like-wise we have green valleys, lovely water falls etc.  It is rich in religious and historical heritages. Lumbini, Gorkha,   Janakpur, Kathmandu are the famous examples.');

-- --------------------------------------------------------

--
-- Table structure for table `table_2`
--

CREATE TABLE `table_2` (
  `id` int(6) DEFAULT NULL,
  `passage` text,
  `correct` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_2`
--

INSERT INTO `table_2` (`id`, `passage`, `correct`) VALUES
(12, 'So this is the text involved in question no 12. Here we have our first hidden text which will only be visible if we press the button given as follows.\r\n<br>\r\n<div id=\"first\"><b>Wilst du bis der todd auch sclieden! Sie lieben auchen sleichten tagen! Nein!</b></div><form action= \"practice_main.php\"  method=\"post\" name=\"myForm\"><input type=\"submit\" class=\"button\" id=\"toggle1\" value=\"1\" name=\"send\"></form><br>\r\nHope this worked. Here is another hidden text with the button. This time, notice that if the first text is showing and we click this button, the first button will be hidden.<br>\r\n<div id=\"second\"><b>Trei ihr sien di allen tage! Nein. Amerikanski infantry!!!</b></div><form action= \"practice_main.php\" method=\"post\" name=\"myForm\"><input type=\"submit\" class=\"button\" id=\"toggle2\" value=\"2\" name=\"send\"></form><br><br>\r\nSimilarly here is another button to show another hidden text.<br>\r\n<div id=\"third\"><b>Wie alt bis du? Ich bin fundfuzwangiz jahre alt.</b></div><form action= \"practice_main.php\" method=\"post\" name=\"myForm\"><input type=\"submit\" class=\"button\" id=\"toggle3\" value=\"3\" name=\"send\"></form><br><br>\r\nAnd the last one. <div id=\"fourth\"><b>Was machen sie beruflich? Ich bin ein lehrer.</b></div><form action= \"practice_main.php\" method=\"post\" name=\"myForm\"><input type=\"submit\" class=\"button\" id=\"toggle4\" value=\"4\" name=\"send\"></form><br><br>?>', 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_2_test`
--

CREATE TABLE `table_2_test` (
  `id` int(6) DEFAULT NULL,
  `passage` text,
  `correct` int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_2_test`
--

INSERT INTO `table_2_test` (`id`, `passage`, `correct`) VALUES
(12, 'So this is the text involved in question no 12. Here we have our first hidden text which will only be visible if we press the button given as follows.\r\n<br>\r\n<div id=\"first\"><b>Wilst du bis der todd auch sclieden! Sie lieben auchen sleichten tagen! Nein!</b></div><form action= \"test_main.php\"  method=\"post\" name=\"myForm\"><input type=\"submit\" class=\"button\" id=\"toggle1\" value=\"1\" name=\"send\"></form><br>\r\nHope this worked. Here is another hidden text with the button. This time, notice that if the first text is showing and we click this button, the first button will be hidden.<br>\r\n<div id=\"second\"><b>Trei ihr sien di allen tage! Nein. Amerikanski infantry!!!</b></div><form action= \"test_main.php\" method=\"post\" name=\"myForm\"><input type=\"submit\" class=\"button\" id=\"toggle2\" value=\"2\" name=\"send\"></form><br><br>\r\nSimilarly here is another button to show another hidden text.<br>\r\n<div id=\"third\"><b>Wie alt bis du? Ich bin fundfuzwangiz jahre alt.</b></div><form action= \"test_main.php\" method=\"post\" name=\"myForm\"><input type=\"submit\" class=\"button\" id=\"toggle3\" value=\"3\" name=\"send\"></form><br><br>\r\nAnd the last one. <div id=\"fourth\"><b>Was machen sie beruflich? Ich bin ein lehrer.</b></div><form action= \"test_main.php\" method=\"post\" name=\"myForm\"><input type=\"submit\" class=\"button\" id=\"toggle4\" value=\"4\" name=\"send\"></form><br><br>?>', 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_3`
--

CREATE TABLE `table_3` (
  `id` int(6) DEFAULT NULL,
  `question` text,
  `ans1` text,
  `ans2` text,
  `ans3` text,
  `ans4` text,
  `ans5` text,
  `ans6` text,
  `ans7` text,
  `ans8` text,
  `ans9` text,
  `correct1` text,
  `correct2` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_3`
--

INSERT INTO `table_3` (`id`, `question`, `ans1`, `ans2`, `ans3`, `ans4`, `ans5`, `ans6`, `ans7`, `ans8`, `ans9`, `correct1`, `correct2`) VALUES
(13, 'dummy question', 'drag no 1', 'drag no 2', 'drag no 3', 'drag no 4', 'drag no 5', 'drag no 6', 'drag no 7', 'drag no 8', 'drag no 9', 'drag no 1drag no 2drag no 3', 'drag no 4drag no 5drag no 6');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `quanda`
--
ALTER TABLE `quanda`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
